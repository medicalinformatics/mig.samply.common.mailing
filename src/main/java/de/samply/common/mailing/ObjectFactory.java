//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.10-b140802.1033 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.10.06 um 10:58:20 AM CEST 
//

package de.samply.common.mailing;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java element interface
 * generated in the de.samply.common.mailing package.
 *
 * <p>An ObjectFactory allows you to programatically construct new instances of the Java
 * representation for XML content. The Java representation of XML content can consist of schema
 * derived interfaces and classes representing the binding of schema type definitions, element
 * declarations and model groups. Factory methods for each of these are provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private static final QName _MailSending_QNAME =
        new QName("http://schema.samply.de/config/MailSending", "mailSending");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes
     * for package: de.samply.common.mailing
     */
    public ObjectFactory() {}

    /**
     * Create an instance of
     * {@link MailSending }.
     */
    public MailSending createMailSending() {
        return new MailSending();
    }

    /**
     * Create an instance of
     * {@link JaxbElement }{@code <}{@link MailSending }{@code >}}.
     */
    @XmlElementDecl(namespace = "http://schema.samply.de/config/MailSending", name = "mailSending")
    public JAXBElement<MailSending> createMailSending(MailSending value) {
        return new JAXBElement<>(_MailSending_QNAME, MailSending.class, null, value);
    }
}
