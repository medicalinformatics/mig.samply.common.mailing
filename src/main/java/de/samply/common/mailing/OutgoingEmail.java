/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.common.mailing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class OutgoingEmail {

  public static final String LOCALE_LEY = "locale";
  private final HashMap<String, String> parameters;
  private final ArrayList<String> addressees;
  private String subject;
  private List<Address> replyTo;
  private List<Address> ccRecipients;
  private EmailBuilder emailBuilder;

  /**
   * TODO: add javadoc.
   */
  public OutgoingEmail() {
    addressees = new ArrayList<>();
    replyTo = new ArrayList<>();
    ccRecipients = new ArrayList<>();
    parameters = new HashMap<>();
    parameters.put(LOCALE_LEY, "en");
  }

  /**
   * Sets the locale of the email, passed to the emailBuilder.
   *
   * @param locale a locale
   */
  public void setLocale(String locale) {
    parameters.put(LOCALE_LEY, locale);
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  /**
   * TODO: add javadoc.
   */
  public void addReplyTo(String address) {
    try {
      replyTo.add(new InternetAddress(address));
    } catch (AddressException e) {
      throw new IllegalArgumentException(e.getMessage());
    }
  }

  public List<Address> getReplyTo() {
    return replyTo;
  }

  /**
   * TODO: add javadoc.
   */
  public Address[] getReplyToArray() {
    Address[] replyToArray = new Address[replyTo.size()];
    replyToArray = replyTo.toArray(replyToArray);
    return replyToArray;
  }

  /**
   * TODO: add javadoc.
   */
  public void addCcRecipient(String address) {
    try {
      ccRecipients.add(new InternetAddress(address));
    } catch (AddressException e) {
      throw new IllegalArgumentException(e.getMessage());
    }
  }

  public List<Address> getCcRecipient() {
    return ccRecipients;
  }

  /**
   * Gets the text of the email.
   *
   * @return the text of the email
   */
  public String getText() {
    if (emailBuilder == null) {
      throw new IllegalStateException("No Builder set for generating the text of the email.");
    }
    return emailBuilder.getText(parameters);
  }

  public EmailBuilder getBuilder() {
    return emailBuilder;
  }

  /**
   * Sets the email builder for generating the mail's text.
   *
   * @param emailBuilder the new email builder instance to set
   */
  public void setBuilder(EmailBuilder emailBuilder) {
    this.emailBuilder = emailBuilder;
  }

  /**
   * Adds an email address that this mail is sent to.
   *
   * @param emailAddress the email address of the new addressee
   */
  public void addAddressee(String emailAddress) {
    addressees.add(emailAddress);
  }

  public List<String> getAddressees() {
    return addressees;
  }

  /**
   * Sets a parameter that is used in the Email.
   *
   * @param key the parameter key
   * @param value the parameter value
   */
  public void putParameter(String key, String value) {
    parameters.put(key, value);
  }
}
